/*
 * CSc212 Project 1: (queue implementation)
 * 
 * References:
 * 	Jean Pena
 * 	---------
 * 		unsigned long getBack();
 * 		unsigned long getSize();
 * 		bool isFull();
 * 		bool push(unsigned long);
 * 		unsigned long pop();
 * 		void erase();
 * 	Professor Skeith
 * 	----------------
 * 		The rest of queue.cpp
 */

/* This is the implementation file for the queue, where you will define how the
 * abstract functionality (described in the header file interface) is going to
 * be accomplished in terms of explicit sequences of C++ statements. */

#include "queue.h"
#include <stdio.h>

/*Note that we will use the following "class invariants" for our data members:
1. frontIndx is the index of the element at the front of the queue, if any.
2. nextBackIndx is the index that is one after the back of the queue.
3. the elements data[frontIndx...nextBackIndx-1] are the valid elements
	of the queue, where each index in the range [frontIndx...nextBackIndx-1]
	is reduced modulo qArraySize ("modulo" === remainder === %-operator in C)
4. the empty queue will be represented by frontIndx == nextBackIndx
As a result of invariant 4, we must have that the queue is full when
nextBackIndx is one away from frontIndx: otherwise an insertion will make
the queue appear to be empty.  Note that "away from" in the preceding sentence
doesn't necessarily mean "one less" due to the circular way we use the array.*/

namespace csc212
{
	queue::queue()
	{
		/* initialize the queue to be empty. */
		this->nextBackIndx = 0;
		this->frontIndx = 0;
	}

	queue::~queue()
	{
		/* since we did not dynamically allocate any memory,
		 * there is nothing for us to release here.  */
	}

	unsigned long queue::getBack() //Worst running time: O(1)
	{
		//returns the element at the back of the queue
		//note: if the precondition is not met, then this function's
		//return value will not have the desired meaning, if it has any at all.
		if(!this->isEmpty())return this->data[(this->nextBackIndx + csc212::qArraySize - 1) % csc212::qArraySize]; //just so it compiles...
		else return 0;
	}
	unsigned long queue::getFront()
	{
		return this->data[this->frontIndx];
		//see the remarks for getBack
	}
	unsigned long queue::getCapacity()
	{
		return csc212::qArraySize - 1;
	}

	unsigned long queue::getSize() //Worst running time: O(1)
	{
		//returns the number of elements in the queue
		return (this->nextBackIndx + csc212::qArraySize - this->frontIndx) % csc212::qArraySize; //just so it compiles...
	}

	bool queue::isEmpty()
	{
		return (this->nextBackIndx == this->frontIndx);
	}

	bool queue::isFull() //Worst running time: O(1)
	{
		//returns true if the queue is full and false if it's not
		return ((this->nextBackIndx +1) % csc212::qArraySize == this->frontIndx); //just so it compiles...
	}

	bool queue::push(unsigned long v) //Worst running time: O(1)
	{
		//Puts an unsigned long, v, at the end of the queue if possible and increment the back index by 1
		//returns true for success and false for failure
		bool pushable = (!this->isFull());
		if(pushable)
		{
			this->data[this->nextBackIndx++] = v;
			this->nextBackIndx %= csc212::qArraySize;
		}
		return pushable; //just so it compiles...
	}

	unsigned long queue::pop() //Worst running time: O(1)
	{
		//if the queue if not empty, remove the first element, increment the front index by 1, and return the element
		//Otherwise, return 0...
		if(!this->isEmpty()){
		unsigned long popQ = this->data[this->frontIndx++];
		this->frontIndx %= csc212::qArraySize;
		return popQ; //just so it compiles...
		}
		else return 0;
	}

	void queue::erase() //Worst running time: O(1)
	{
		//clear the queue by settingt the back index equal to the front index
		this->nextBackIndx = this->frontIndx;
	}

	ostream& operator<<(ostream& o, const queue& q)
	{
		unsigned long i;
		for(i=q.frontIndx; i!=q.nextBackIndx; i=(i+1) % csc212::qArraySize)
			o << q.data[i] << "  ";
		return o;
	}
	void queue::print(FILE* f)
	{
		for(unsigned long i=this->frontIndx; i!=this->nextBackIndx;
				i=(i+1) % csc212::qArraySize)
			fprintf(f, "%lu ",this->data[i]);
		fprintf(f, "\n");
	}
}
