/****************************************************
implementation file for our binary search tree.
*****************************************************/

/* References:
 * 	Jean Pena
 * 	---------
 * 		Tree::Tree(const Tree& T);
 * 		Tree::void operator=(const Tree& T);
 * 		treeNode* copyST(treeNode* original);
 * 		bool searchST(treeNode* stroot, val_type x);
 * 		void removeST(treeNode*& stroot, val_type x);
 * 		void clearST(treeNode*& stroot);
 * 		val_type maxST(treeNode* stroot);
 * 		val_type minST(treeNode* stroot);
 * 		void inOrderST(treeNode*& stroot, nodeProcessingFn f, void* pParams);
 * 		void postOrderST(treeNode*& stroot, nodeProcessingFn f, void* pParams);
 * 		unsigned long numLeavesST(treeNode* stroot);
 * 		unsigned long sizeST(treeNode* stroot);
 * 	Professor Skeith
 * 	----------------
 * 		The rest of searchTree.cpp
 */

#include <iostream>
#include <cmath>
#include <iomanip>
#include <cstdio>

using std::cin;
using std::cout;
using std::endl;
using std::setw;

#include "searchTree.h"

namespace csc212
{

	struct treeNode {
		treeNode(val_type d=0, treeNode* l=0, treeNode* r=0);

		val_type data; /* the data of the node */
		treeNode* left; /* pointer to the left subtree */
		treeNode* right; /* pointer to the right subtree */

		/* please ignore this -- used for printing the tree */
		void getLevel(val_type* A, size_t l, size_t h=0, size_t id=0);
	};

	/* function pointers... */
	typedef void (*nodeProcessingFn)(treeNode*&,void*);
	/* this says that things of type "nodeProcessingFn" are actually functions
	 * which take a treeNode pointer by reference along with another pointer
	 * (for parameters) and doesn't return a value.
	 * */

	/* example: for basic, space-separated printing you could use this: */
	void coutData(treeNode*& n, void*)
	{
		cout << n->data << " ";
	}
	/* a more general function to output data from a node to a stream: */
	void outputData(treeNode*& n, void* pOStream)
	{
		/* NOTE: pOStream is a pointer to the desired output stream */
		std::ostream* o;
		if(pOStream) //if an explicit output stream is supplied, use it:
			o = reinterpret_cast<std::ostream*>(pOStream);
		else //default to using cout
			o = &cout;
		(*o) << n->data << " ";
	}
	/* for smily-face delimited printing: */
	void coutData2(treeNode*& n, void*) { cout << n->data << ": )"; }

	/* for writing a dot file: */
	void printDot(treeNode*& n, void* pFILE)
	{
		/* NOTE: the void* param is assumed to be a file stream which
		 * is opened for writing.  Also, you need to do this post order. */
		if (n==0) return;
		FILE* f = (FILE*)(pFILE);
		fprintf(f, "  \"%p\" [label=%li]\n", n,n->data);
		if (!(n->left||n->right)) return;
		/* nodes with a single child may be rendered directly below, so
		 * print invisible nodes to space it out: */
		if (!n->left) {
			fprintf(f, "  \"%s%p\" [label=\"\",width=.1,style=invis]","l",n);
			fprintf(f, "  \"%p\" -> \"%s%p\" [style=invis]\n",n,"l",n);
		} else {
			fprintf(f, "  \"%p\" -> \"%p\" [style=dashed]\n",n,n->left);
		}
		if (!n->right) {
			fprintf(f, "  \"%s%p\" [label=\"\",width=.1,style=invis]","r",n);
			fprintf(f, "  \"%p\" -> \"%s%p\" [style=invis]\n",n,"r",n);
		} else {
			fprintf(f, "  \"%p\" -> \"%p\"\n",n,n->right);
		}
	}

	/* forward declarations for subtree functions: */

	/* this makes a copy of the subtree and returns a pointer to the root
	 * (the root of the copy, of course) */
	treeNode* copyST(treeNode* original);

	/* returns whether or not x is in the subtree with root == stroot */
	bool searchST(treeNode* stroot, val_type x);

	
	/* insert and remove for a subtree */
	/* NOTE: the pointers for these two are by reference */

	/* attempts to insert x into the subtree, returning whether or not
	 * insertion took place */
	bool insertST(treeNode*& stroot, val_type x);
	/* attempts to remove x from the subtree */
	void removeST(treeNode*& stroot, val_type x);

	/* clears a subtree. */
	void clearST(treeNode*& stroot);

	/* min and max for a subtree */
	val_type maxST(treeNode* stroot);
	val_type minST(treeNode* stroot);

	/* subtree traversal functions */
	/* NOTE: pParams will point to the parameters for f (if there are any) */
	void preOrderST(treeNode*& stroot, nodeProcessingFn f, void* pParams);
	void inOrderST(treeNode*& stroot, nodeProcessingFn f, void* pParams);
	void postOrderST(treeNode*& stroot, nodeProcessingFn f, void* pParams);

	/* tells if this node is a leaf. */
	bool isLeaf(treeNode* tn);

	/* tree statistics */
	/* counts and returns the number of leaves in the subtree */
	unsigned long numLeavesST(treeNode* stroot);
	/* returns the total size of the subtree (i.e., the number of elements) */
	unsigned long sizeST(treeNode* stroot);
	/* gives the height of this subtree */
	unsigned long heightST(treeNode* stroot);

	bool isLeaf(treeNode* tn) { return (tn->left == 0 && tn->right == 0); }

	treeNode* copyST(treeNode* original) //Worst case run time: O(n)
	{
		//return null if this node is null
		if(original == 0) return 0;
		//return a new treenode with 2 null subtrees if this is a leaf
		else if(isLeaf(original)) return new treeNode(original->data, 0, 0);
		//return a new treenode with subtrees from recursive call if this is a parent node.
		else return new treeNode(original->data, copyST(original->left), copyST(original->right));
	}

	/* construction / destruction */
	Tree::Tree() { root = 0; }
	Tree::Tree(const Tree& T) { this->root = copyST(T.root); }
	Tree::Tree(val_type* A, unsigned long size)
	{
		this->root = 0;
		//just sequentially insert items...
		for(unsigned long i=0; i<size; i++)
			this->insert(A[i]);
	}
	treeNode::treeNode(val_type d, treeNode* l, treeNode* r)
		: data(d), left(l), right(r) {}
	Tree::~Tree() { clear(); }

	void Tree::operator=(const Tree& T) //Worst and best case run time: O(n), Omega(1)
	{
		if(this != &T) // only do this if the Tree isn't being assigned to itself
		{
			this->clear(); // delete this Tree
			this->root = copyST(T.root); // copy Tree T. Tree doesn't have a copy function...
		}
	}

	bool Tree::isEmpty() { return (root==0); }

	bool searchST(treeNode* stroot, val_type x) //Worst case runtime: O(n)
	{
		// return false if null node was encountered(x was not found)
		if(stroot == 0) return false;
		// return true if x was found here
		else if(x == stroot->data) return true;
		//if x is greater than this treeNode.data, return whether it was found in the right subtree
		else if(x > stroot->data) return searchST(stroot->right, x);
		//if x is else than this treeNode.data, return whether it was found in the left subtree
		else return searchST(stroot->left, x);
	}

	bool Tree::search(val_type x)
	{
		return searchST(this->root,x);
	}

	/* insert x into subtree. returns !search(x) (i.e., whether or not an
	 * insert took place) */
	bool insertST(treeNode*& stroot, val_type x)
	{
		if(stroot == 0)
		{
			stroot = new treeNode(x);
			return true;
		}
		if(x < stroot->data)
			return insertST(stroot->left,x);
		else if(stroot->data < x)
			return insertST(stroot->right,x);
		return false; //x was already in the tree
	}
	bool Tree::insert(val_type x) { return insertST(this->root,x); }

	val_type maxST(treeNode* stroot) //worst case runtime: O(n)
	{
		//if the right subtree is null, this is the largest value in the tree, so return it.
		if(stroot->right == 0) return stroot->data;
		//otherwise, look for the max in the right subtree
		else return maxST(stroot->right);
		return 0;
	}
	val_type Tree::max() { return maxST(this->root); }

	val_type minST(treeNode* stroot) //returns min value in subtree. Worst case runtime: O(n)
	{
		//if the left subtree is null, this is the smallest value in the tree, so return it.		
		if(stroot->left == 0) return stroot->data;
		//otherwise, look for the min in the left subtree
		else return minST(stroot->left);
	}
	val_type Tree::min() { return minST(this->root); }

	void removeST(treeNode*& stroot, val_type x) //worst case runtime: O(n)
	{
		//Only do this if this subtree in not null
		if(stroot != 0)
		{
			//if x > this node's data, try to remove x from the right subtree
			if(x > stroot->data) removeST(stroot->right, x);
			//if x < this node's data, try to remove x from the left subtree
			else if(x < stroot->data) removeST(stroot->left, x);
			else // x == stroot->data. remove x
			{
				//store the pointer to this subtree
				treeNode* doomed = stroot;
				//if it's a leaf, just set the pointer to null and free up memory
				if(isLeaf(stroot))
				{
					stroot = 0;
					delete doomed;
				}
				//if it only has a right subtree, set this pointer to the right subtree and free up memory
				else if(stroot->left == 0)
				{
					stroot = stroot->right;
					delete doomed;
				}
				//if it only has a left subtree, set this pointer to the left subtree and free up memory
				else if(stroot->right == 0)
				{
					stroot = stroot->left;
					delete doomed;
				}
				else //there are 2 children. Copy the right's min to this node and apply delete to the right's mins
				{
					stroot->data = minST(stroot->right);
					removeST(stroot->right, stroot->data);
				}
			}
		}
	}
	void Tree::remove(csc212::val_type x)
	{
		removeST(this->root,x);
	}

	void clearST(treeNode*& stroot) //Worst case runtime: O(n)
	{
		//only do this if this subtree isn't already empty
		if(stroot != 0)
		{
			//recursively delete left and right subtrees
			clearST(stroot->left);
			clearST(stroot->right);
			
			//After subtrees have been deleted, delete this one.
			
			//store pointer to this subtree
			treeNode* doomed = stroot;
			//assign null to this subtree pointer
			stroot = 0;
			//now delete subtree
			delete doomed;
		}
	}
	void Tree::clear()
	{
		clearST(this->root);
	}

	/* traversals */
	void preOrderST(treeNode*& stroot, nodeProcessingFn f, void* pParams)
	{
		//root, left, right
		//I'll do one for you to help you get the idea.
		if(stroot==0) return;
		f(stroot,pParams); //apply some process (f) to the root.
		preOrderST(stroot->left,f,pParams); //then process the left subtree
		preOrderST(stroot->right,f,pParams);//and then the right subtree
	}
	void inOrderST(treeNode*& stroot, nodeProcessingFn f, void* pParams) //Worst case runtime: O(n)
	{
		//left, root, right
		//Copy and pasted and reordered to meet the needs of this function.
		if(stroot==0) return;
		inOrderST(stroot->left,f,pParams); //process the left subtree
		f(stroot,pParams); //then apply some process (f) to the root. T(1)
		inOrderST(stroot->right,f,pParams);//and then the right subtree
	}
	void postOrderST(treeNode*& stroot, nodeProcessingFn f, void* pParams) //Worst case runtime: O(n)
	{
		//left, right, root
		//Copy and pasted and reordered to meet the needs of this function.
		if(stroot==0) return;
		postOrderST(stroot->left,f,pParams); //process the left subtree
		postOrderST(stroot->right,f,pParams);//and then the right subtree
		f(stroot,pParams); //then apply some process (f) to the root.
	}

	void Tree::preOrder(ostream* o)
	{
		preOrderST(this->root,&outputData,(void*)(o));
	}
	void Tree::inOrder(ostream* o)
	{
		inOrderST(this->root,&outputData,(void*)(o));
	}
	void Tree::postOrder(ostream* o)
	{
		postOrderST(this->root,&outputData,(void*)(o));
	}

	unsigned long numLeavesST(treeNode* stroot) //returns number of leaves in the subtree. Worst case runtime: O(n)
	{
		//return 0 if this subtree is null
		if(stroot == 0) return 0;
		//return 1 if this subtree is a leaf
		else if(isLeaf(stroot)) return 1;
		//for all other cases, return the sum of leaf nodes in the left and right subtrees.
		else return (numLeavesST(stroot->left) + numLeavesST(stroot->right));
	}
	unsigned long Tree::numLeaves()
	{
		return numLeavesST(this->root);
	}

	/* return size of subtree */
	unsigned long sizeST(treeNode* stroot)//worst case runtime: O(n)
	{
		//return 0 if this subtree is null
		if(stroot == 0) return 0;
		//otherwise, count left and right subtree nodes and this one too.
		else return (sizeST(stroot->left) + sizeST(stroot->right) + 1);
		return 0;
	}
	unsigned long Tree::size()
	{
		return sizeST(this->root);
	}

	unsigned long heightST(treeNode* stroot) //gives the height of this subtree
	{
		if(stroot == 0) return 0;
		if(isLeaf(stroot)) //a single node has height 0
			return 0;

		unsigned long hl = 0, hr = 0; //left and right subtree heights
		hl = heightST(stroot->left);
		hr = heightST(stroot->right);
		if(hl < hr)
			return 1+hr;
		return 1+hl;
	}
	unsigned long Tree::height() { return heightST(this->root); }

	void Tree::drawDot(const char* fname)
	{
		FILE* fdot = fopen(fname,"wb");
		fprintf(fdot, "digraph bstree {\n");
		fprintf(fdot, "  graph [ordering=\"out\"];\n");
		fprintf(fdot, "  bgcolor=black\n  edge [color=white]\n");
		fprintf(fdot, "  node [style=filled color=white fillcolor=dodgerblue4 shape=circle]\n");
		postOrderST(this->root,&printDot,fdot);
		fprintf(fdot, "}\n");
		fclose(fdot);
	}
	void treeNode::getLevel(val_type* A, size_t l, size_t h, size_t id)
	{
		if(l==h)
		{
			A[id] = data;
			return;
		}
		h++;
		if(left)
			left->getLevel(A,l,h,2*id);
		if(right)
			right->getLevel(A,l,h,2*id+1);
	}
	void printSpace(int n)
	{
		for(int i=0; i<n; i++)
			cout << ' ';
	}
	void Tree::drawTree()
	{
		if(this->isEmpty()) return;
		unsigned long h = height() + 1;
		val_type m = max();
		unsigned long len = 1;
		unsigned long n = (unsigned long)pow((double)2,(int)h-1);
		/* (this is number of nodes in bottom row.) */
		while((m = m/10)) //set len = to log of the max
			len++;
		val_type* A = new val_type[n];
		unsigned long i;
		cout << endl << endl;
		for(i=0; i<h; i++)
		{
			for(unsigned long k=0; k<pow((double)2,(int)i); k++)
				A[k] = -1;
			root->getLevel(A,i);
			for(unsigned long j=0; j<pow((double)2,(int)i); j++)
			{
				printSpace((int)(n/(pow((double)2,(int)i)))*len);
				if(i>0 && j%2) 
					printSpace(2*(h+len-i-3)+1);
				if(A[j]>=0)
					cout << setw(len) << A[j];
				else
					printSpace(len);
			}
			cout << endl << endl;
		}
		cout << endl << endl;

		delete [] A;
	}
	ostream& operator<<(ostream& o, Tree& T)
	{
		preOrderST(T.root,&outputData,(void*)(&o));
		return o;
	}
}
