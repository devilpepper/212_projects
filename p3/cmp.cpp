#include <fstream>
using std::ofstream;
using std::endl;

#include <iostream>
using std::cout;
using std::cerr;

#include <string.h>
#include <string>
#include <algorithm>
using std::string;

void normalizeLines(std::string& s)
{
	size_t pos = 0;
	std::string creturn = "\r";
	//erase all the carriage returns.
	while ((pos = s.find(creturn,pos)) != std::string::npos) {
		s.erase(pos,creturn.length());
	}
}


int main(){
	char* sout, *out;
	std::ofstream o("o"), s("s"), ol("ol"), sl("sl");
	
	FILE* fsout = fopen("soutput","r");
	if(fsout == 0) {
		cout << "File \"soutput\" not found to test against.\n\n";
		return -1;
	}
	FILE* fout = fopen("output","r");
	if (fout == 0) {
		cout << "File \"\" not found.\n\n";
		return -2;
	}
	//else, both files exist.  Read them.
	unsigned long ssize,osize;
	fseek(fsout,0,SEEK_END);
	ssize = ftell(fsout);
	fseek(fsout,0,SEEK_SET);
	fseek(fout,0,SEEK_END);
	osize = ftell(fout);
	fseek(fout,0,SEEK_SET);
	sout = new char[ssize+1];
	out = new char[osize+1];
	fread(sout,1,ssize,fsout);
	fread(out,1,osize,fout);
	fclose(fsout);
	fclose(fout);
	//make sure they are c-strings:
	sout[ssize] = 0;
	out[osize] = 0;
	//now tokenize and compare.	

	char* token; //the current token
	std::string answers[256]; //holds the answers. upper bound the answers by 256 for now.
	std::string results[256]; //holds the submitted answers
	char delims[] = "@"; //just the @ sign for now
	token = ::strtok(sout,delims);
	unsigned long nAnswers=0,j=0;
	while(token)
	{
		answers[nAnswers] = token;
		normalizeLines(answers[nAnswers++]);
		token = ::strtok(0,delims);
	}
	token = ::strtok(out,delims);
	while(token && j<nAnswers)
	{
		results[j] = token;
		normalizeLines(results[j++]);
		token = ::strtok(0,delims);
	}
	for(unsigned long i=0; i<nAnswers; i++)
	{
		sl<<answers[i];
		ol<<results[i];
		std::replace(answers[i].begin(), answers[i].end(), ' ', '\n');
		std::replace(results[i].begin(), results[i].end(), ' ', '\n');
		s<<answers[i];
		o<<results[i];
	}

}
