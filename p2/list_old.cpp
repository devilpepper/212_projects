/****************************************************
implementation file for linked list class.
*****************************************************/

#include "list.h"
#include <stdlib.h> //for rand()

namespace csc212
{
	/* listNode stuff: */
	listNode::listNode()
	{
		this->next = 0; //set the next pointer to be null
	}

	listNode::listNode(val_type x, listNode *pNext)
	{
		this->data = x;
		this->next = pNext;
	}


	/* linked list implementation: */
	list::list()
	{
		this->root = 0; //initialize the list to be empty
		//remember 0 == NULL, which is never memory you own.
	}

	list::list(const list &L)
	{
		//we need to make *this a copy of L
		listNode* pL, *p; //pL will traverse L, p will traverse *this
		pL = L.root;
		root = 0; //start out with the empty list
		if(L.root != 0)
		{
			this->root = new listNode(pL->data);
			pL = pL->next;
		}
		p = this->root;
		//loop invariant:

		/*pL is a pointer to the next node to be copied in L
		p is a pointer to the last node created in (*this)
		in english: pL goes through L and is is one step ahead of p,
		which is going through (*this)
		our loop should add a new node to *this, and then maintain the invariant*/

		while(pL != 0) //pL != NULL
		{
			p->next = new listNode(pL->data);
			p = p->next;
			pL = pL->next;
		}
	}

	list::~list()
	{
		this->clear(); //delete all nodes, deallocating memory
	}

	list& list::operator=(const list& L)
	{
		//idea: erase our contents, then copy L, similar to the copy constructor.
		//but first, check for self-assignment:
		if(this == &L) return *this;

		clear();
		listNode* pL, *p;
		pL = L.root;
		if(pL)
		{
			root = new listNode(pL->data);
			pL = pL->next;
		}
		p = root;
		while(pL)
		{
			p->next = new listNode(pL->data);
			p = p->next;
			pL = pL->next;
		}
		return *this;
	}

	bool operator==(const list& L1, const list& L2)
	{
		/* TODO: write this. */
		return false;  //just so it compiles.  you of course need to do something different.
	}
	bool operator !=(const list& L1, const list& L2)
	{
		return !(L1==L2);
	}

	void list::insert(val_type x)
	{
		/* TODO: write this. */
	}

	void list::remove(val_type x)
	{
		/* TODO: write this. */
	}

	bool list::isEmpty()
	{
		return (this->root == 0);
	}

	void list::clear()
	{
		//idea: repeatedly delete the root node...
		listNode* p;
		while((p = root)) //yes, I do mean "=", not "=="
		{
			root = p->next;
			delete p;
		}
	}

	ostream& operator<<(ostream& o, const list& L)
	{
		listNode* p;
		p = L.root;
		while(p)
		{
			o << p->data << " ";
			p = p->next;
		}
		return o;
	}

	bool list::search(val_type x) const
	{
		listNode* p;
		p = root;
		while(p && p->data != x) //again, short circuit evaluation is important...
			p = p->next;
		if(p) 
			return true;
		else 
			return false;
	}

	unsigned long list::length() const
	{
		/* TODO: write this. */
		return 0; //just so it compiles.  you of course need to do something different.
	}

	void list::merge(const list& L1, const list& L2)
	{
		/* TODO: write this. */
		//this algorithm should run in LINEAR TIME and set *this
		//to be the union of L1 and L2, and furthermore the list should remain sorted.
	}

	void list::randomFill(unsigned long n, unsigned long k)
	{
		//we want to fill the list with n random integers from 0..k-1
		this->clear(); //reset to the empty list
		unsigned long i;
		for(i=0; i<n; i++)
		{
			this->insert((val_type)(rand()%k));
		}
	}

	void list::intersection(const list &L1, const list& L2)
	{
		/* TODO: write this. */
		//this algorithm should run in LINEAR TIME, setting *this
		//to be the intersection (ordered) of L1 and L2.
	}
}
