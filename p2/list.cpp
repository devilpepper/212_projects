/****************************************************
implementation file for linked list class.
*****************************************************/

/* References:
 * 	Jean Pena
 * 	---------
 * 		friend bool operator==(const list& L1, const list& L2);
 * 		void insert(val_type x);
 * 		void remove(val_type x);
 * 		unsigned long length() const;
 * 		void merge(const list& L1, const list& L2);
 * 		void intersection(const list& L1, const list& L2);
 * 	Professor Skeith
 * 	----------------
 * 		The rest of list.cpp
 */

#include "list.h"
#include <stdlib.h> //for rand()

namespace csc212
{
	/* listNode stuff: */
	listNode::listNode()
	{
		this->next = 0; //set the next pointer to be null
	}

	listNode::listNode(val_type x, listNode *pNext)
	{
		this->data = x;
		this->next = pNext;
	}


	/* linked list implementation: */
	list::list()
	{
		this->root = 0; //initialize the list to be empty
		//remember 0 == NULL, which is never memory you own.
	}

	list::list(const list &L)
	{
		//we need to make *this a copy of L
		listNode* pL, *p; //pL will traverse L, p will traverse *this
		pL = L.root;
		root = 0; //start out with the empty list
		if(L.root != 0)
		{
			this->root = new listNode(pL->data);
			pL = pL->next;
		}
		p = this->root;
		//loop invariant:

		/*pL is a pointer to the next node to be copied in L
		p is a pointer to the last node created in (*this)
		in english: pL goes through L and is is one step ahead of p,
		which is going through (*this)
		our loop should add a new node to *this, and then maintain the invariant*/

		while(pL != 0) //pL != NULL
		{
			p->next = new listNode(pL->data);
			p = p->next;
			pL = pL->next;
		}
	}

	list::~list()
	{
		this->clear(); //delete all nodes, deallocating memory
	}

	list& list::operator=(const list& L)
	{
		//idea: erase our contents, then copy L, similar to the copy constructor.
		//but first, check for self-assignment:
		if(this == &L) return *this;

		clear();
		listNode* pL, *p;
		pL = L.root;
		if(pL)
		{
			root = new listNode(pL->data);
			pL = pL->next;
		}
		p = root;
		while(pL)
		{
			p->next = new listNode(pL->data);
			p = p->next;
			pL = pL->next;
		}
		return *this;
	}

	bool operator==(const list& L1, const list& L2) //Worst running time: Theta(n)
	{
		//Calling length() guarentees n additional run time.
		//Instead, check if pL1 = pL2 = null at the end.
		bool equality4all = true;//(L1.length() == L2.length());
		listNode* pL1 = L1.root;
		listNode* pL2 = L2.root;

		//If either list is empty, loop doesn't run, neither list will be traversed, n = 0: Omega(n)
		//If lists are equal, loop runs n/2 times iterating through both lists at the same time: O(n);
		//loop may end if a non equal data is encountered or either loop reached it's end.
		while((pL1 != 0 && pL2 != 0) && (equality4all &= (pL1->data == pL2->data)))
		{
			pL1 = pL1->next;
			pL2 = pL2->next;
		}
		//Check if L1 = L2 (null), in case one list was shorter than the other. i.e.:
		//L1 = {1, 2, 3, 4}
		//L2 = {1, 2, 3}
		//Would have returned (L1 == L2) = true without this line!! DX
		equality4all &= (pL1 == pL2);
		return equality4all;  //just so it compiles.  you of course need to do something different.
	}
	bool operator !=(const list& L1, const list& L2)
	{
		return !(L1==L2);
	}

	void list::insert(val_type x) //Worst running time: Omega(1), O(n)
	{
		
		listNode** p2p = &(this->root);
		//Traverse list until a value greater than x is encountered
		//by dereferencing p2p and checking that listNode's data against x
		//In the best case, the first element in the list is greater than x,
		//so we don't need to traverse anything: Omega(1)
		//In the worst case, the list contains only values smaller than x, so it's traversed to the end: O(n)
		while(*p2p != 0 && x > (*p2p)->data) p2p = &((*p2p)->next);
		//Take the value of the pointer p2p points to and put it in a new listNode
		//then make the pointer point to the new listNode.
		*p2p = new listNode(x, *p2p);

#if 0
		//pointer to pointer is way better
		if(this->isEmpty())
		{
			this->root = new listNode(x);
		}
		else
		{
			
			listNode* p = this->root;
			if(x<this->root->data)this->root = new listNode(x, this->root);
			else
			{
				while(p->next != 0 && x > p->next->data) p = p->next;
				p->next = new listNode(x, p->next);
			}
		}
#endif
	}

	void list::remove(val_type x) //Worst running time: Omega(1), O(n)
	{
		listNode** p2p = &(this->root);
		listNode* tempP;
		//Traverse list until x is encountered
		//by dereferencing p2p and checking that listNode's data against x
		//In the best case, x is the first element and we don't need to traverse anything: Omega(1)
		//In the worst case, the list is not empty and doesn't contain x, so it's traversed to the end: O(n)
		while(*p2p != 0 && (*p2p)->data != x) p2p = &(*p2p)->next;
		//if x was found, p2p shouldn't be pointing to a null pointer
		if(*p2p != 0)
		{
			//Copy the value of current listNode's next pointer
			tempP = (*p2p)->next;
			//delete the current listNode
			delete *p2p;
			//Replace the value of the current pointer with the address of the deleted listNode's next pointer
			*p2p = tempP;
		}
	}

	bool list::isEmpty()
	{
		return (this->root == 0);
	}

	void list::clear()
	{
		//idea: repeatedly delete the root node...
		listNode* p;
		while((p = root)) //yes, I do mean "=", not "=="
		{
			root = p->next;
			delete p;
		}
	}

	ostream& operator<<(ostream& o, const list& L)
	{
		listNode* p;
		p = L.root;
		while(p)
		{
			o << p->data << " ";
			p = p->next;
		}
		return o;
	}

	bool list::search(val_type x) const
	{
		listNode* p;
		p = root;
		while(p && p->data != x) //again, short circuit evaluation is important...
			p = p->next;
		if(p) 
			return true;
		else 
			return false;
	}

	unsigned long list::length() const //Worst running time: Theta(n)
	{
		//counter
		unsigned long x = 0;
		listNode* p = this->root;
		//traverse entire list and increment the counter.
		//if root = null, n = 0
		//otherwise, loop runs n times. Omega(n) && O(n) => Theta(n)
		while(p)//!null
		{
			p = p->next;
			x++;
		}
		return x;
	}

	void list::merge(const list& L1, const list& L2) //Worst running time: Theta(n)
	{
		//this algorithm should run in LINEAR TIME and set *this
		//to be the union of L1 and L2, and furthermore the list should remain sorted.
		//
		//This function runs in n = L1.length() + L2.length() time
		//unless this isn't empty. In that case, it will run in n + m time
		//The loop will run, comparing and iterating through data,
		//copying the smallest ones first, and ends when one list's end was reached
		//Then a second loop will copy the remaining elements in the other list
		if(this->root != 0) this->clear();
		listNode **p = &(this->root), *pL1 = L1.root, *pL2 = L2.root;

		//While both ends haven't been reached
		while(pL1 != 0 && pL2 !=0)
		{
			//If pL1 data < pL2 data, copy pL1 data to this and move up in pL1
			if(pL1->data < pL2->data)
			{
				*p = new listNode(pL1->data);
				pL1 = pL1->next;
			}
			//If pL2 data <= pL1 data, copy pL2 data to this and move up in pL2
			else
			{
				*p = new listNode(pL2->data);
				pL2 = pL2->next;
			}
			//move to this's next pointer
			p = &((*p)->next);
		}
		//if list L1's end was reached, recycle pL1.
		//otherwise L2 end was reached and we could just continue using pL1 as is
		if (pL1 == 0) pL1 = pL2;
		
		//Copy remaining elements
		while(pL1 != 0)
		{
			*p = new listNode(pL1->data);
			pL1 = pL1->next;
			p = &((*p)->next);
		}
#if 0 //this was redundant and the null pointer could be reused instead
		while(pL2 != 0)
		{
			*p = new listNode(pL2->data);
			pL2 = pL2->next;
			p = &((*p)->next);
		}
#endif
	}

	void list::randomFill(unsigned long n, unsigned long k)
	{
		//we want to fill the list with n random integers from 0..k-2
		this->clear(); //reset to the empty list
		unsigned long i;
		for(i=0; i<n; i++)
		{
			this->insert((val_type)(rand()%k));
		}
	}

	void list::intersection(const list &L1, const list& L2) //Worst running time: Omega(1), O(n)
	{
		//this algorithm should run in LINEAR TIME, setting *this
		//to be the intersection (ordered) of L1 and L2.
		//
		//Since n = L1.length() + L2.length(), in the worst case,
		//when the lists are the same length, the loop iterates n/2 times: O(n)
		//When the lists are not the same length, the loop iterates as long as
		//the smaller list, and because one can be empty, the loop might never run: Omega(1)
		//If this list isn't empty, it would be a good idea to clear it first, so run time is m + n/2
		if(this->root != 0) this->clear();

		listNode **p2p = &(this->root), *pL1 = L1.root, *pL2 = L2.root;

		//while both lists ends haven't been reached
		while(pL1 != 0 && pL2 !=0)
		{
			//if these 2 elements match, add it to this list and point to all 3 next listNodes
			if(pL1->data == pL2->data)
			{
				*p2p = new listNode(pL1->data);
				p2p = &((*p2p)->next);
				pL1 = pL1->next;
				pL2 = pL2->next;
			}
			//if pL1 data > pL2 data, move to the next pL2 data
			else if(pL1->data > pL2->data) pL2 = pL2->next;
			//if pL1 data < pL2 data, move to the next pL1 data
			else pL1 = pL1->next;
		}
	}
}
