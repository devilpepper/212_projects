/* interface for graph class. */

/* How to represent the graph is up to you.
 * You might consider using STL vectors or lists.
 * */

/* Class invariant:
 * 1. The graph class contains a list of vertices.
 * 2. It is directed or undirected.
 * 3. Each vertex has a value and a list of pointers
 * 		to neighboring vertices.
 * 4. Vertices and edges are unique
 * 5. The class is written by Jean Pena
 * */

#pragma once
#include <map>
using std::map;
#include <algorithm>
using std::find;
#include <list>
using std::list;
#include<queue>
using std::queue;
#include<iostream>
using std::cout;

namespace csc212
{
	typedef int val_type;
	
	struct vertex{
		/*So graph has access to it's vertices' members*/
		friend class graph;
		private:
			/*member variables*/
			
			val_type data;
			//map of neighbor vertices
			map<val_type, vertex*> neighbor;
		public:
			/*constructors*/
			
			vertex(val_type val=0);
			vertex(const vertex& v);
		
			/*operator overloading*/
			
			void operator=(const vertex& v);
			//defines vertex=val_type
			void operator=(val_type x);
			//compares vertex.data with val_type
			bool operator==(const val_type& x);

			/*accessor functions*/

			//returns vertex.data
			val_type getData() const;
			//returns read-only reference to vertex.neighbor
			const map<val_type, vertex*>& getNeighbors() const;
	};

	class graph
	{
		private:
			/*member variables*/

			bool undir;
			val_type to;
			val_type from;

			list<vertex> vertices;
			
			map<vertex*, vertex*> bfsTree;
			map<vertex*, vertex*> dfsTree;
			map<vertex*, vertex*> path;
			map<vertex*, int> distance;
			map<vertex*, int> start;
			map<vertex*, int> finish;
			list<list<vertex*> > SCC;//Strongly connected components

			/*private functions*/

			void realDFS(vertex* src, map<vertex*, vertex*>& tree, int& times);
			//returns a reference to the vertex containing x
			//creates one if it isn't found
			vertex& ffind(val_type x);
			//copies the stuff in G to this
			void copyG(const graph& G);
		public:
			/*constructors*/
			
			graph(bool undirected = false, val_type src = 0, val_type dest = 0);
			graph(const graph& G);

			/*operator overloading*/

			graph& operator=(const graph& G);

			/*accessor functions*/

			//returns whether this graph is undirected or not
			bool isUndirected() const;

			//return whether x is in a strongly connected component or not
			bool inSCC(val_type x);

			//returns read-only reference to graph.vertices
			const list<vertex>& getVertices() const;

			//return read-only reference to graph.dfsTree
			const map<vertex*, vertex*>& getdfsTree() const;

			//adds an edge from src to dest on the graph. Adds vertices if needed
			void addEdge(val_type src, val_type dest);
			//makes G^T
			graph transpose();

			//does bfs
			void bfs();
			//does dfs
			void dfs(val_type src = 0);
			//finds strongly connected components
			void components();
			//finds the shortest path from src to dest
			void pathTo(val_type src, val_type dest);

			//does what it says
			void clearMaps();
			//outputs dot file of G using cout
			void toDot();
	};
}
