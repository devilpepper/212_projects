/* huffman encoding. */
/*
 * Jean Pena
 * =========
 * Used Piazza discussions to help find a problem with
 * assigning the return value of vector.pop_back() //void
 * and fix heap functions.
 * (Thought they were max heaps, but that didn't make sense)
 * Also, looked at vector.push_back, back(), and pop_back() complexity:
 * http://www.cplusplus.com/reference/vector/vector/push_back/#complexity
 * http://www.cplusplus.com/reference/vector/vector/back/#complexity
 * http://www.cplusplus.com/reference/vector/vector/pop_back/#complexity
 */
#pragma once
#include <stddef.h> /* size_t */
#include <iostream>
using std::ostream;
#include <fstream>
using std::ofstream;
#include <algorithm>
using std::swap;
#include <functional>
using std::less;
#include <map>
using std::map;
#include <vector>
using std::vector;
#include <string>
using std::string;

/* forward declarations: */
template <typename T> struct hNode;
template <typename T>
void nodeToDot(hNode<T>* n, void* pFILE);

/* node for huffman encoding tree */
template <typename T>
struct hNode {
	T symbol;
	size_t weight;
	hNode<T>* left;
	hNode<T>* right;
	hNode() {left = right = 0;}
	typedef void (*nodeProcFn)(hNode<T>*,void*);
	void postOrderST(nodeProcFn f, void* pParams) {
		if(this == 0) return;
		left->postOrderST(f,pParams);
		right->postOrderST(f,pParams);
		f(this,pParams);
	}
	void drawDot(const char* fname) {
		ofstream fdot(fname);
		fdot << "digraph htree {\n" <<
				"  graph [ordering=\"out\"];\n" <<
				"  bgcolor=black\n" <<
				"  edge [color=white fontcolor=white]\n" <<
				"  node [style=filled color=white " <<
				"fillcolor=plum shape=circle]\n";
		this->postOrderST(&nodeToDot,(void*)&fdot);
		fdot << "}\n";
		fdot.close();
	}
};

/* a few utility functions for trees: */
template <typename T>
void nodeToDot(hNode<T>* n, void* pfdot)
{
	/* NOTE: the void* param is assumed to be a file stream which
	 * is opened for writing.  Also, you need to do this post order. */
	if (n==0) return;
	ofstream& fdot = *(ofstream*)(pfdot);
	/* print out leaf nodes with symbol and weight; print internal
	 * nodes with different color, and only weight. */
	if (!n->left && !n->right) {
		fdot << "  \"" << n << "\" [label=\"" << n->symbol <<
			" | " << n->weight <<"\" shape=box fillcolor=palegreen3]\n";
	} else {
		fdot << "  \"" << n << "\" [label=\"" << n->weight <<"\"]\n";
	}
	if (!(n->left||n->right)) return;
	/* for a Huffman tree, should be "full" (each node either
	 * has both children, or is a leaf) */
	if (n->left) {
		fdot << "  \""<<n<<"\" -> \""<<n->left<<"\" [label=0]\n";
	}
	if (n->right) {
		fdot << "  \""<<n<<"\" -> \""<<n->right<<"\" [label=1]\n";
	}
}

template <typename T>
void printLabels(hNode<T>* n, string& label)
{
	if (n==0) return;
	/* leaf node: print the symbol and label;
	 * non-leaf: append 0 for left, 1 for right. */
	if (!n->left && !n->right) {
		cout << "  " << n->symbol << "\t" << label << "\n";
		return;
	}
	label.push_back('0');
	printLabels(n->left,label);
	label.erase(label.size()-1);
	label.push_back('1');
	printLabels(n->right,label);
	label.erase(label.size()-1);
}

/*************** Heap functions ***************/
/* NOTE: most heap operations can be done in-place on an array.
 * Hence, we don't bother making a heap class, but instead just
 * provide a set of functions for arrays. */

/* utility macros for finding left / right and parent, assuming
 * 0-based indexing. */
#define LEFT(x)  ((x<<1)+1)
#define RIGHT(x) ((x+1)<<1)
#define PARENT(x) ((x-1)/2)

/* we want to compare pointers to hNodes by their weight: */
template <typename T>
struct hnComp {
	bool operator()(hNode<T>* n1, hNode<T>* n2) {return (n1->weight < n2->weight);}
};

/* heapify (downward). assumes that both left and right children of i
 * are max heaps (so that the only possible violation is at i). */
template <typename T, typename L>
void heapify(vector<T>& A, size_t i, L lt = less<T>()) //Worst case runtime: O(log(n))
{
	size_t n = A.size();
	if(LEFT(i) < n)//not leaf node
	{
		size_t child = LEFT(i);
		//RIGHT(i) = LEFT(i) + 1
		//if the right child exists and it is smaller than the left child
		//make the right child the one we want to swap
		if(child+1 < n && lt(A[child+1], A[child])) child++;
		//swap if the child is indeed smaller than this node and heapify down.
		if(lt(A[child], A[i]))
		{
			swap(A[i], A[child]);
			heapify(A, child, lt);
		}
	}
}

/* we also need to heapify "upward" when inserting new elements: */
template <typename T, typename L>
void heapify_up(vector<T>& A, size_t i, L lt = less<T>()) //Worst case runtime: O(log(n))
{
	if(i != 0)//not root node
	{
		size_t parent = PARENT(i);
		//if this node is smaller than it's parent, swap and heapify up.
		if(lt(A[i], A[parent]))
		{
			swap(A[parent], A[i]);
			heapify_up(A, parent, lt);
		}
	}
	
}

/* buildHeap.  Takes an un-ordered array and turns it into a heap. */
template <typename T, typename L>
void buildHeap(vector<T>& A, L lt = less<T>()) //Worst case runtime: O(n)
{
	/* Note that all leaves are heaps on their own.  So, you can work
	 * backwards through the tree calling heapify, starting from the
	 * last node's parent. */
	int i = PARENT(A.size()-1); //start at last node's parent
	while (i >= 0) heapify(A, i--, lt); //move left along the tree and heapify
}

/************* priority queue functions. **************/
/* NOTE: we assume that every array passed to these functions is already a
 * heap!  NOTE: although the operations take only log(n) time, this is not
 * the most efficient priority queue implementation known -- see your book
 * for more advanced techniques which can reduce the *insert* time over a
 * sequence of calls.  Finally, note that the smallest element will always
 * be stored at index 0. */

/* remove and return front item from priority queue. */
template <typename T, typename L>
T pqPop(vector<T>& Q, L lt = less<T>()) //Worst case runtime: O(log(n))
{
	/* copy last value to top and reduce size (call pop_back). */
	
	T value = Q[0]; //copy the top for return value
	Q[0] = Q.back(); //copy last value to top.
	Q.pop_back(); //pop_back().
	heapify(Q, 0, lt); //heapify down from root node
	return value; /* just so it compiles! */
}

/* insert new item into priority queue. */
template <typename T, typename L>
void pqPush(vector<T>& Q, T x, L lt = less<T>()) //Worst case runtime: O(n)
{
	/* push new item to back; then heapify up. */
	Q.push_back(x); //O(n)... amortized constant runtime. If resize is necessary, it's the worst case.
	heapify_up(Q, (Q.size() - 1), lt);
}

/************* huffman encoding / decoding *************/

/* Take raw data and encode.  Return the root of the tree used
 * for the encoding.  (This would be needed if we ever wanted
 * to decode.)
 * NOTE: this function should allocate memory for nodes (as an array)
 * which the caller is then responsible for cleaning up.  See the
 * readme for more details. */
template <typename T>
hNode<T>* huffmanEncode(T* data, size_t len, hNode<T>*& nodes) //Worst case runtime: O(n)
{
	/* step 1: make a frequency table. */
	/* step 2: create array of nodes.  Note that 2n will suffice (this
	 * follows from the fact that a Huffman tree will always be full).
	 * step 3: initialize array from frequency table. */

	/* since we don't want the nodes to ever be re-located (which would
	 * invalidate the pointers in the Huffman tree), we build our queue
	 * out of node pointers rather than the nodes themselves. */
	/* step 4: call buildHeap to create a priority queue; then build the
	 * tree by repeatedly "merging" the two nodes at the front of the
	 * queue, and then reinserting the merged node. */
	
	map<T, int> f; //here's the frequency table and it's allocation
	for(size_t i=0; i<len; i++) f[data[i]]++;
	size_t n = 2 * f.size(), j=0; //"Note that 2n will suffice..." j is an iterator
	
	nodes = new hNode<T>[n]; //allocate space for 2n hNodes for caller to clean up

	//Copied from readme.html
	vector<hNode<T>*> pq;
	hnComp<T> lt; /* defines how to compare node pointers */
	
	//set each symbol and it's frequency to the jth hNode's symbol and weight
	//push a pointer to the jth Node into the vector pq
	for(typename map<T,int>::iterator i=f.begin(); i!=f.end(); i++){
		nodes[j].symbol = i->first;
		nodes[j].weight = i->second;
		pq.push_back(&nodes[j++]);
	}

	//buid priority queue from pq (also copied form readme.html
	buildHeap(pq,lt); //T(n) = O(n)
	
	//While there are more than 1 pointer in pq,
	//pop the top 2 elements(smallest weight)
	//merge them together in one of the excess hNodes(the jth one)
	//set the new hNode's weight to the sum of it's children's weights
	//finally, push it back into the priority queue
	while(pq.size() > 1) //T(n) = O(n) because vector resize will not happen
	{
		nodes[j].left = pqPop(pq, lt);
		nodes[j].right = pqPop(pq, lt);
		nodes[j].weight = nodes[j].left->weight + nodes[j].right->weight;
		pqPush(pq, &nodes[j++], lt);
	}
	//in case we are making a huffman code out of nothing, return null, if so.
	if(pq.size() == 0) pq.push_back(0);
	return pq[0];
}
