/* Generate random graphs. */
#include <cstdio>
#include <cstdlib>
#include <getopt.h>
#include <time.h>
#include <vector>
using std::vector;

static const char* usage =
"Usage: %s [OPTIONS]...\n"
"Print random graph to stdout.\n\n"
"   -n,--size     NUM    number of vertexes.\n"
"   -c,--ncomps   NUM    number of SCC's (directed graph only).\n"
"   -p,--edgeprob NUM    sets prob[(i,j)\\in G] = NUM. (undirected graph).\n"
"   --undir              generate undirected graph.\n"
"   --dotfmt             output in dot format.\n"
"   --help               show this message and exit.\n";

#define LOG(x) (sizeof(x)*8 - __builtin_clzl(x))

/* Use the stochastic block model to generate a graph with
 * "interesting" strongly connected components.
 * The idea is to make random small graphs (distributed like G(n,p))
 * and then arrange some connections between those, making
 * a DAG if you look at the connections between blocks.  G(n,p) can
 * be tuned to almost always have one SCC.  Then if the number of
 * nodes in block i is k and j has h, and if p=P(i,j), we have
 * (if implemented as below), (1-p)^{kh} probability of not connecting
 * the DAG edge (block i --> block j) that you hoped for.  Thus, you can
 * get away with a small value of p, and thus end up with the desired
 * properties, yet not too many "unnecessary" edges cluttering the graph.
 * */
int randDirected(size_t n, size_t sc)
{
	/* phase 1, use evenly sized components:
	 * Block(i,n,s) = (i*s)/n
	 * */
	fprintf(stderr, "n:\t%lu\nsc:\t%lu\nbs:\t%lu\n",n,sc,n/sc);
	/* matrix for the blocks: */
	vector<vector<double> > P(sc,vector<double>(sc));
	for (size_t i = 0; i < sc; i++) {
		P[i][i] = 0.5;
		for (size_t j = 0; j < i; j++) {
			P[i][j] = 0.0;
			P[j][i] = (1.0*sc)/n; /* shrink as n/sc grows */
		}
	}
	/* now actually make the connections. */
	for (size_t i = 0; i < n; i++) {
		for (size_t j = 0; j < n; j++) {
			if (i!=j && ((double)rand())/RAND_MAX < P[(i*sc)/n][(j*sc)/n])
				printf("%lu %lu\n",i,j);
		}
	}
	return 0;
}

/* Erdos-Renyi G(n,p) */
int randUndir(size_t n, double p)
{
	for (size_t i = 0; i < n; i++) {
		for (size_t j = 0; j < i; j++) {
			if (((double)rand())/RAND_MAX < p)
				printf("%lu %lu\n",i,j);
		}
	}
	return 0;
}

int main(int argc, char *argv[])
{
	int undir = false;
	int dotfmt = false;
	struct option long_opts[] = {
		{"size",      required_argument, 0,      'n'},
		{"ncomps",    required_argument, 0,      'c'},
		{"edgeprob",  required_argument, 0,      'p'},
		{"undir",     no_argument,       &undir,  1},
		{"dotfmt",    no_argument,       &dotfmt, 1},
		{"help",      no_argument,       0,      'h'},
		{0,0,0,0}
	};
	char c;
	int opt_index = 0;
	size_t n = 15;
	size_t ncomps = 0;
	double eprob = -1;
	while ((c = getopt_long(argc, argv, "hn:m:d", long_opts, &opt_index)) != -1) {
		switch (c) {
			case 'h':
				printf(usage,argv[0]);
				return 0;
			case 'n':
				n = atol(optarg);
				break;
			case 'c':
				ncomps = atol(optarg);
				break;
			case 'p':
				eprob = atof(optarg);
				break;
			case '?':
				printf(usage,argv[0]);
				return 1;
		}
	}
	if (eprob < 0) {
		eprob = ((double)LOG(n))/n;
	}
	if (ncomps == 0) {
		ncomps = LOG(n);
	}

	srand(time(0));
	if (undir) {
		randUndir(n,eprob);
	} else {
		randDirected(n,ncomps);
	}

	return 0;
}
