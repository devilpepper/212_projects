/****************************************************
implementation file for the hash table class.
*****************************************************/

/* References:
 * 	Jean Pena
 * 	---------
 * 		hashTbl::hashTbl(const hashTbl& H);
 * 		hashTbl::hashTbl& operator=(const hashTbl& H);
 * 		void hashTbl::insert(val_type x);
 * 		void hashTbl::remove(val_type x);
 * 		bool hashTbl::search(val_type x) const;
 * 		size_t hashTbl::countElements() const;
 * 		size_t hashTbl::longestListLength() const;
 * 		uint64_t hashTbl::hash::operator()(const string& x) const;
 * 	Professor Skeith
 * 	----------------
 * 		The rest of hashtbl.cpp, reinterpret cast and, memcpy
 */


#include "hashtbl.h"
#include <iostream>
using std::endl;
using std::ostream;
#include <string.h> /* for strcpy */
#include <algorithm> /* for find */
using std::find;
#include <iomanip>
using std::setfill;
using std::setw;
#include <cassert>

#define R32 (rand()*100003 + rand())
#define R64 (((uint64_t)R32 << 32) + R32)
#define TLEN ((size_t)(1 << this->nBits))

namespace csc212
{
	/* First we implement our hash family */
	hashTbl::hash::hash(unsigned rangebits, const uint32_t* a,
					const uint64_t* alpha, const uint64_t* beta) {
		this->rangebits = rangebits;
		if (a) {
			for (size_t i = 0; i < aLen; i++) {
				this->a[i] = a[i] | 1; /* make sure it is odd. */
			}
		} else {
			for (size_t i = 0; i < aLen; i++) {
				this->a[i] = R32 | 1;
			}
		}
		this->alpha = ((alpha) ? *alpha : R64) | 1;
		this->beta = ((beta) ? *beta : R64);
		/* only keep the low order bits of beta: */
		this->beta &= ((uint64_t)-1)>>(64 - rangebits);
	}
	uint64_t hashTbl::hash::operator()(const string& x) const { //Worst case runtime: O(n)  (depends only on string length)
		assert(x.length() <= 4*aLen);

		//sum variable
		uint64_t hx=0;
		unsigned long xL = x.length(), full;
		//# of complete dwords
		full = xL/8;

		//s points to the same address as x, but is interpreted as a pointer to integer(s)
		const uint32_t* s = reinterpret_cast<const uint32_t*>(x.c_str());

		//h(x) = Sigma(s_2i + a_2i)(s_(2i+1) + a_(2i+1)) mod 2^64, for (i=0; i<(x.length()/2); i++)
		for(unsigned long i=0; i<(full); i++)
			hx+=(uint64_t)(s[2*i] + this->a[2*i])*(uint64_t)(s[(2*i + 1)] + this->a[2*i + 1]);
		
		//if there was an incomplete dword.
		if((xL%=8) != 0)
		{
			//x.length()/8 * 2 so I don't need to keep multiplying by 2
			full *= 2;
			//2 element array to store the last 1-7 bytes padded with 0s
			uint32_t last[2] = {0,0};
			//copy remaining bytes from the string
			memcpy(last, x.c_str() + (4*full), xL);

			//final iteration of summation
			hx+=(uint64_t)(last[0] + this->a[full])*(uint64_t)(last[1] + this->a[full + 1]);
		}
		//g(h(x)) = (alpha*h(x) + beta) / 2^(64-m)
		return ((this->alpha)*hx + (this->beta)) >> (64-(this->rangebits));
	}

	//constructors:
	hashTbl::hashTbl(unsigned nBits, const uint32_t* ha,
					const uint64_t* halpha, const uint64_t* hbeta) :
		nBits(nBits),h(nBits,ha,halpha,hbeta)
	{
		this->table = new list<val_type>[TLEN];
	}
	hashTbl::hashTbl(const hashTbl& H) //Worst case run time: O(n)
	{
		/* NOTE: the underlying linked list class has a working
		 * assignment operator! */

		//delete any exiting table and hash function
		this->nBits = H.nBits;
		//this->h = *(new hash(unsigned(H.h.rangebits),(H.h.a),&(H.h.alpha),&(H.h.beta)));
		this->h = H.h;
		this->table = new list<val_type>[TLEN];

		//copy every table element from H
		for(size_t i=0; i<TLEN; i++) this->table[i] = H.table[i];
	}

	//destructor:
	hashTbl::~hashTbl()
	{
		delete[] this->table;
		//NOTE: this will call the destructor of each of the linked lists,
		//so there isn't anything else that we need to worry about.
	}

	//operators:
	hashTbl& hashTbl::operator=(const hashTbl& H)//Worst case runtime: O(n)
	{
		if(this == &H)
		{
			//delete any exiting table
			delete[] this->table;

			//copy nBits and hash function from H, and create a new table
			this->nBits = H.nBits;
			this->h = H.h;//*(new hash(unsigned(H.h.rangebits),(H.h.a),&(H.h.alpha),&(H.h.beta)));
			this->table = new list<val_type>[TLEN];
		
			//copy every table element from H
			for(size_t i=0; i<TLEN; i++) this->table[i] = H.table[i];
		}
		return *this;
	}

	ostream& operator<<(ostream& o, const hashTbl& H)
	{
		for (size_t i = 0; i < H.tableLength(); i++) {
			o << "[" << setfill('0') << setw(2) << i << "] |";
			for (list<val_type>::iterator j = H.table[i].begin();
					j != H.table[i].end(); j++) {
				o << *j << "|";
			}
			o << endl;
		}
		return o;
	}

	void hashTbl::insert(val_type x)//Worst case runtime: O(n) (if all elements are on table[h(x)]. Very unlikely)
	{
		//Remember to check for uniqueness before inserting.
		
		//Compute h(x)
		//std::cout<<"Computing h(x)\n";
		uint64_t hx = this->h(x);
		//at table[h(x)], first check if x exists in the table. If not, push it to the back of the list.
		//std::cout<<"Searching for x in table[h(x)]\n"; //crashes searching for 64 character string
		if (!(find(this->table[hx].begin(),this->table[hx].end(),x) != this->table[hx].end())){
			//std::cout<<"Inserting x in table[h(x)]\n";
			this->table[hx].push_front(x);
			//std::cout<<"x was inserted\n";
		}
	}

	void hashTbl::remove(val_type x)//Worst case runtime: O(1)
	{
		//Compute h(x)
		uint64_t hx = this->h(x);
		//removes every instance of x from table[h(x)] and decrements the table size accordingly (should just be 1)
		this->table[hx].remove(x);
	}

	void hashTbl::clear()
	{
		for(size_t i=0; i<TLEN; i++)
			this->table[i].clear();
	}

	bool hashTbl::isEmpty() const
	{
		/* look for any non-empty list. */
		for(size_t i=0; i<TLEN; i++)
			if(!this->table[i].empty()) return false;
		return true;
	}

	bool hashTbl::search(val_type x) const //Worst case runtime: O(n) (if all elements are on table[h(x)]. Very unlikely)
	{
		//Compute h(x)
		uint64_t hx = this->h(x);
		//return whether or not x was found in table[h(x)]
		return (find(this->table[hx].begin(),this->table[hx].end(),x) != this->table[hx].end());
	}

	size_t hashTbl::countElements() const //Worst case runtime: O(1)
	{
		//counter
		size_t ele=0;
		//add each table's size to counter
		for(size_t i=0; i<TLEN; i++) ele += table[i].size();
		return ele;
	}

	size_t hashTbl::tableLength() const
	{
		return TLEN;
	}

	size_t hashTbl::countCollisions() const
	{
		//just count the number of lists of length > 1
		size_t i,nCollisions=0;
		for(i=0; i<TLEN; i++)
			if(table[i].size() > 1)
				++nCollisions;
		return nCollisions;
	}

	size_t hashTbl::longestListLength() const //Worst case runtime: O(1)
	{
		//storage for longest list length and current list length
		size_t longest=0, tbl;
		for(size_t i=0; i<TLEN; i++)
			//this this one is larger than the ones before, it's a new record!
			if((tbl = table[i].size()) > longest) longest = tbl;
		return longest;
	}
}
