/* implementation of graph class. */

/* Jean Pena */
#include "graph.h"

namespace csc212
{
	/*vertex stuff */
	vertex::vertex(val_type val):data(val){}
	
	vertex::vertex(const vertex& v)
		{this->data = v.getData();}

	void vertex::operator=(const vertex& v)
	{
		if(this != &v)
		{
			this->data = v.getData();
			neighbor.clear();
		}
	}

	//probably not useful at all
	void vertex::operator=(val_type x)
	{
		this->data = x;
		neighbor.clear();
	}

	bool vertex::operator==(const val_type& x)
		{return (this->data == x);}

	val_type vertex::getData() const{return (this->data);}

	const map<val_type, vertex*>& vertex::getNeighbors()const {return this->neighbor;}

	/*graph stuff*/
	graph::graph(bool undirected, val_type src, val_type dest):undir(undirected),to(dest),from(src){}
	
	graph::graph(const graph& G){this->copyG(G);}

	graph& graph::operator=(const graph& G)
	{
		if(this != &G)
		{
			this->copyG(G);
		}
		return *this;
	}

	bool graph::isUndirected() const{return this->undir;}

	bool graph::inSCC(val_type x)
	{
		bool found = false;
		list<list<vertex*> >::iterator L;
		list<vertex*>::iterator sL;
		//look for x in all of the SCC
		for(L = this->SCC.begin(); L != this->SCC.end() && !found; L++)
			for(sL = L->begin(); sL != L->end() && !found; sL++)
				found |= ((*sL)->data == x);
		//return whether it was found or not
		return found;
	}

	const list<vertex>& graph::getVertices() const{return this->vertices;}

	const map<vertex*, vertex*>& graph::getdfsTree() const{return this->dfsTree;}

	void graph::addEdge(val_type src, val_type dest)
	{
		//force find src and dest
		vertex &vSrc = this->ffind(src), &vDest = this->ffind(dest);
		
		map<val_type, vertex*>::iterator k = vSrc.neighbor.begin();
		while((k != vSrc.neighbor.end()) && !((*(k->second)) == dest)) k++;
		//create a new edge by adding destination to the
		//list of neighbors if it doesn't exist
		if(k == vSrc.neighbor.end())
			vSrc.neighbor[dest] = &vDest;

		if(this->undir)//create reverse edge as well if undirected
		{
			k = vDest.neighbor.begin();
			while((k != vDest.neighbor.end()) && !((*(k->second)) == src)) k++;
			//create a new edge by adding destination to the
			//list of neighbors if it doesn't exist
			if(k == vDest.neighbor.end())
				vDest.neighbor[src] = &vSrc;
		}
	}

	void graph::bfs()
	{
		list<vertex>::iterator i;
		//only do bfs if the source exists
		if(((i=find(this->vertices.begin(),
			this->vertices.end(), this->from)) != this->vertices.end()))
		{
			queue<vertex*> Q; //queue of nodes to process

			Q.push(&*i); //push
			this->distance[&*i] = 0; //distance of source from source is 0
			vertex *v;
			map<int, vertex*>::iterator u;
			while(!Q.empty())
			{
				//pop queue
				v = Q.front();
				Q.pop();
				//for all the neighbors
				for(u = v->neighbor.begin(); u != v->neighbor.end(); u++)
				{
					//if neighbor wasn't visited yet
					if(this->distance.find(u->second) == this->distance.end()){
						//distance of neighbor from source should
						//be distance of this node from source +1
						this->distance[u->second] = this->distance[v]+1;
						this->bfsTree[u->second] = v; //print pair.second -> pair.first
						//push neighbor to queue
						Q.push(u->second);
					}
				}
			}

		}
	}

	void graph::dfs(val_type src)
	{
		list<vertex>::iterator i;
		//check if src exists
		if(((i=find(this->vertices.begin(),
			this->vertices.end(), src)) != this->vertices.end()))
		{
			//start navigation time at 0
			int times = 0;
			//dfs tree root node is the one containing the source
			this->dfsTree[&*i] = 0;
			//call the real DFS function
			this->realDFS(&*i, this->dfsTree, times);
		}
	}

	void graph::components()
	{	
		map<vertex*, vertex*>::iterator ai;
		list<vertex>::iterator v = this->vertices.begin();
		//Do DFS once
		int times = 0;
		this->dfsTree[&*v] = 0;
		this->realDFS(&*v, this->dfsTree, times);
		map<vertex*, vertex*> accessible(this->dfsTree);

		//Do DFS on the rest of the graph until all nodes were visited
		for(v++; v != this->vertices.end(); v++)
		{
			//if node wasn't visited do dfs starting there
			if(accessible.find(&*v) == accessible.end())
			{
				accessible[&*v] = 0;
				this->realDFS(&*v, accessible, times);
			}
		}
		
		map<int, vertex*> finishSorted;
		map<vertex*, int>::iterator fT;
		map<int, vertex*>::reverse_iterator rfT;
		//sort by finish time
		for(fT = this->finish.begin(); fT != this->finish.end(); fT++)
			finishSorted[fT->second] = fT->first;

		//G^T
		graph this2theT = this->transpose();
		list<vertex*> temp;

		//do DFS on G^T with sorted vertices
		for(rfT = finishSorted.rbegin(); rfT != finishSorted.rend(); rfT++)
		{
			//if this vertex isn't already in a strongly connected component
			if(!this->inSCC(rfT->second->data))
			{
				//do dfs starting with this vertex in G^T
				this2theT.dfs(rfT->second->data);
				accessible.clear();
				//copy the stuff
				accessible = this2theT.getdfsTree();
				//push a new list<vertex*>
				this->SCC.push_back(temp);
				//get a reference to the new list
				list<vertex*> &thisList = this->SCC.back();
				//copy all vertices in accessible that aren't
				//already in a SCC
				for(ai = accessible.begin(); ai != accessible.end(); ai++)
					if(!this->inSCC(ai->first->data))
						thisList.push_back(&ffind(ai->first->data));
				//pretty much reset G^T
				this2theT.clearMaps();
			}
		}
		this->start.clear();
		this->finish.clear();
	}

	void graph::pathTo(val_type src, val_type dest)
	{
		//a modified version of bfs
		list<vertex>::iterator i;
		//if src and dest exist
		if(((i=find(this->vertices.begin(),
			this->vertices.end(), src)) != this->vertices.end())
				&& find(this->vertices.begin(),
					this->vertices.end(), dest) != this->vertices.end())
		{
			//these should have already been set, but just in case...
			this->to = dest;
			this->from = src;
			queue<vertex*> Q; //queue of nodes to process

			Q.push(&*i);
			this->path[&*i] = 0; //src node can be the root.
			bool isDest = false; //we want to stop at dest
			vertex *v;
			map<int, vertex*>::iterator u;
			while(!(isDest || Q.empty()))
			{
				v = Q.front();
				Q.pop();
				for(u = v->neighbor.begin(); u != v->neighbor.end() && !isDest; u++)
				{
					if(this->path.find(u->second) == this->path.end())
					{
						//store bfs progress in path.
						this->path[u->second] = v;
						isDest |= (u->second->data == dest);
						Q.push(u->second);
					}
					//shortest path is dest, path[dest], path[path[dest]],....source
					//in reverse
				}
			}
		}
	}

	void graph::clearMaps()
	{
		//does what it says
		this->bfsTree.clear();
		this->dfsTree.clear();
		this->path.clear();
		this->distance.clear();
		this->start.clear();
		this->finish.clear();
	}

	graph graph::transpose()
	{
		graph G2theT(this->undir);//probably shouldn't call transpose if graph is undirected
		list<vertex>::iterator Vi;
		map<val_type, vertex*>::iterator Vf;

		//copy every edge backwards
		for(Vi = this->vertices.begin(); Vi != this->vertices.end(); Vi++)
			for(Vf = Vi->neighbor.begin(); Vf != Vi->neighbor.end(); Vf++)
				G2theT.addEdge(Vf->first, Vi->data);
		return G2theT;
	}
	/*private stuff*/
	vertex& graph::ffind(val_type x)
	{
		//force find
		list<vertex>::iterator v;
		//if x exists, return a reference to the vertex that contains it
		if((v = find(this->vertices.begin(),
			this->vertices.end(), x)) != this->vertices.end()) return *v;
		//else push a new vertex containing it, and return a reference to that
		this->vertices.push_back(vertex(x));
		return this->vertices.back();
	}

	void graph::copyG(const graph& G)
	{
		//copy boolean value
		this->undir = G.isUndirected();
	
		//get a pointer to G's vertices(this doesn't have access to G's privates)
		const list<vertex> &gV = G.getVertices();
		//declare some iterators. u is const_iterator because gV is const
		list<vertex>::iterator v;
		list<vertex>::const_iterator u;
		
		//const map pointer and const_iterator for it
		//Don't want to declare these n times.
		//const map<val_type, vertex*> *gmap;
		map<val_type, vertex*>::const_iterator gAdj;
		
		//for each of G's vertices, make a copy of it in this
		//uses vertex copy constructor
		for(u = gV.begin(); u != gV.end(); u++) 
			this->vertices.push_back(*u); //dereferencing an iterator
				
		//initialize v since u is still the iterator
		v = this->vertices.begin();
		//for each of G's vertices, get it's neighbors and
		//make this's verices have the same neighbors
		for(u = gV.begin(); u != gV.end(); u++)
		{
			const map<val_type, vertex*> &gmap = u->getNeighbors();
			//for each of gV's neighbors, find a corresponding
			//vector in this list and put it in this map of neighbors(pointers)
			for(gAdj = gmap.begin(); gAdj != gmap.end(); gAdj++)
				v->neighbor[gAdj->first] = 
					&*((find(this->vertices.begin(), this->vertices.end(), gAdj->first)));
			//u==v
			//now go to the next v to copy the next u
			v++;
		}
	}
	void graph::realDFS(vertex* src, map<vertex*, vertex*>& tree, int& times)
	{
		//This is the real DFS algorithm. It assumes src exists since it can
		//only be called internally
		list<list<vertex*> > L;
		map<int, vertex*>::iterator u;

		//create a start time label for current vertex
		this->start[src] = times++; //O(Log(n))
		//for every neighbor
		for(u = src->neighbor.begin(); u != src->neighbor.end(); u++)//O(n)
			{
				//if not already visited
				if(tree.find(u->second) == tree.end())
				{
					//means path from src to it's neighbor
					tree[u->second] = src;//O(log(n))
					//run dfs on the neighbor
					this->realDFS(u->second, tree, times);//recursive call
				}
			}
		//create a finish time label for current vertex
		this->finish[src] = times++;
	}
	void graph::toDot()
	{
		cout<<"strict "<<(this->undir ? "" : "di")<<"graph{\n";
		//string shortestPath = "";
		if(this->path.size()>0)
		{
			int d = 0;
			vertex* v = &(this->ffind(this->to)), *u;
			//cout<<"\t"<<v->data;
			u = this->path[v];
			while(v->data != this->from)
			{
				d++;
				cout<<"\t"<<u->data<<(this->undir ? " -- " : " -> ")<<v->data
					<<"[style=dashed];\n";
				v = u;
				u = this->path[u];
				//v = this->path[v];
				//cout<<(this->undir ? " -- " : " -> ")<<v->data;
			}
			//cout<<" [style=dotted];\n";
			cout<<"\t//Shortest distance from "<<this->from<<" to "
				<<this->to<<": "<<d<<" edges\n";
		}
		if(this->SCC.size()>0)
			cout<<"\t//# of strongly connected components: "<<this->SCC.size()<<"\n";

		cout<<"\tnode[colorscheme=paired12];\n"
			<<"\tnode[style=filled];\n"
			<<"\tnode[fillcolor=9];\n";

		map<vertex*, vertex*>::iterator direct;

		//highlight BFS tree before anything
		for(direct = this->bfsTree.begin(); direct != this->bfsTree.end(); direct++)
			if(direct->second != 0)
				cout<<"\t"<<direct->second->data<<(this->undir ? " -- " : " -> ")
				<<direct->first->data<<" [style=dotted];\n";
		
		//highlight DFS tree before anything
		for(direct = this->dfsTree.begin(); direct != this->dfsTree.end(); direct++)
			if(direct->second != 0)
				cout<<"\t"<<direct->second->data<<(this->undir ? " -- " : " -> ")
				<<direct->first->data<<" [style=dotted];\n";
		
		//highlight shortest path before anything
		//cout<<shortestPath;

		list<vertex>::iterator startNode;
		map<val_type, vertex*>::iterator endNode;
		//place all of the edges
		for(startNode = this->vertices.begin(); startNode != this->vertices.end(); startNode++)
			for(endNode = startNode->neighbor.begin(); endNode != startNode->neighbor.end(); endNode++)
				cout<<"\t"<<startNode->data<<(this->undir ? " -- " : " -> ")
					<<endNode->first<<"\n";

		//highlight source and destination
		if(this->path.size()>0)
			cout<<"\t"<<this->from<<"[fillcolor=2]\n"
				<<"\t"<<this->to<<"[fillcolor=2]\n";

		map<vertex*, int>::iterator measured;

		//label distances
		for(measured = this->distance.begin(); measured != this->distance.end(); measured++)
			cout<<"\t"<<measured->first->data<<"[label=\""<<measured->first->data<<"\\nd: "
				<<measured->second<<"\"]\n";
		
		//label start/finish times
		for(measured = this->start.begin(); measured != this->start.end(); measured++)
			cout<<"\t"<<measured->first->data<<"[label=\""<<measured->first->data<<"\\n"
				<<measured->second<<"|"<<this->finish[measured->first]<<"\"]\n";

		int color = 0;
		list<list<vertex*> >::iterator SCClist;
		list<vertex*>::iterator subGraph;

		//color code Strongly connected components
		for(SCClist = this->SCC.begin(); SCClist != this->SCC.end(); SCClist++)
		{
			for(subGraph = SCClist->begin(); subGraph != SCClist->end(); subGraph++)
				cout<<"\t"<<(*subGraph)->data<<"[fillcolor="<<(color%12+1)<<"]\n";
			color++;
		}

		cout<<"}";
	}
}
